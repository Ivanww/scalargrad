import random
from .scalar import Scalar
from .scalar import scalar_check

class ScalarModule(object):
    def __init__(self):
        super(object, self).__init__()
    
    def parameters(self):
        return []
    
    def zero_grad(self):
        for scalar in self.parameters():
            scalar._grad = 0

class ScalarNeuron(ScalarModule):
    def __init__(self, in_scalar, act='relu'):
        super(ScalarNeuron, self).__init__()
        self.weights = [Scalar(random.uniform(-1, 1)) for _ in range(in_scalar)]
        self.bias = Scalar(0.)
        self.act = act
    
    def __call__(self, x):
        logits = sum((w*xi for w, xi in zip(self.weights, x)), self.bias)
        
        if self.act == 'relu':
            return logits.relu()
        elif self.act == 'sigmoid':
            return logits.sigmoid()
        else:
            return logits

    def parameters(self):
        return self.weights+[self.bias]
    
    def __repr__(self):
        return f'{self.act.capitalize()}Neuron({len(self.weights)})'
    
class ScalarLinear(ScalarModule):
    def __init__(self, in_scalar, out_scalar, act='relu'):
        super(ScalarLinear, self).__init__()
        self.in_scalar = in_scalar
        self.out_scalar = out_scalar
        self.neurons = [ScalarNeuron(in_scalar, act) for _ in range(out_scalar)]
    
    def __call__(self, x):
        out = [self.neurons[i](x) for i in range(self.out_scalar)]
        return out[0] if len(out) == 1 else out
        
    def parameters(self):
        return [p for n in self.neurons for p in n.parameters()]
    
    def __repr__(self):
        return f"Linear [{', '.join(str(n) for n in self.neurons)}]\n"

class ScalarConv2d(ScalarModule):
    pass

class ScalarSequentialModule(ScalarModule):
    def __init__(self, modules):
        super(ScalarSequentialModule, self).__init__()
        self.modules = modules

    def __call__(self, x):
        for m in self.modules:
            x = m(x)
        return x
    
    def parameters(self):
        return [p for m in self.modules for p in m.parameters()]
    
    def __repr__(self):
        return f"Sequential of [\n{''.join(str(m) for m in self.modules)}]"

