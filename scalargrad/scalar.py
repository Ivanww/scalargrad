import math

def scalar_check(func):
    def checked_func(*args):
        args = [arg if isinstance(arg, Scalar) else Scalar(arg) for arg in args]
        return func(*args)

    return checked_func

@scalar_check
def op_add(a, b):
    out = Scalar(a._scalar + b._scalar, (a, b), '+')
    def _backward():
        a._grad += out._grad
        b._grad += out._grad
    out._backward = _backward

    return out

@scalar_check
def op_mul(a, b):
    out = Scalar(a._scalar * b._scalar, (a, b), '*')
    def _backward():
        a._grad += out._grad * b._scalar
        b._grad += out._grad * a._scalar
        
    out._backward = _backward

    return out

def op_pow(a, b):
    assert isinstance(b, (int, float))
    a = a if isinstance(a, Scalar) else Scalar(a)

    out = Scalar(a._scalar**b, (a,), f'^{b}')
    def _backward():
        a._grad += (b* a._scalar**(b-1)) * out._grad

    out._backward = _backward

    return out

@scalar_check
def op_relu(a):
    out = Scalar(0 if a._scalar < 0. else a._scalar, (a,), 'ReLU')

    def _backward():
        a._grad += (a._scalar > 0.) * out._grad
    out._backward = _backward

    return out

@scalar_check
def op_sigmoid(a):
    out = Scalar(1 / (1 + math.exp(-a._scalar)), (a,), 'Sigmoid')
    
    def _backward():
        a._grad += (out._scalar * (1 - out._scalar)) * out._grad
    out._backward = _backward
    
    return out

@scalar_check
def op_log(a):
    out = Scalar(math.log(a._scalar), (a, ), 'Log')

    def _backward():
        a._grad += 1 / a._scalar * out._grad
    out._backward = _backward

    return out
    
class Scalar(object):
    def __init__(self, scalar=0, children=(), op=''):
        super(object, self).__init__()
        self._scalar = scalar
        self._grad = 0

        self._backward = lambda: None
        self._children = set(children)
        
        self._op = op

    def __add__(self, other):
        return op_add(self, other)
   
    def __radd__(self, other):
        return op_add(other, self)
    
    def __mul__(self, other):
        return op_mul(self, other)
    
    def __rmul__(self, other):
        return op_mul(self, other)
    
    def __neg__(self):
        return op_mul(self, -1)
    
    def __sub__(self, other):
        return self + (-other)
    
    def __rsub__(self, other):
        return other + (-self)
    
    def __pow__(self, other):
        return op_pow(self, other)
    
    def __truediv__(self, other):
        return self * (other**-1)

    def __rtruediv__(self, other):
        return other * (self**-1)
    
    def relu(self):
        return op_relu(self)
    
    def sigmoid(self):
        return op_sigmoid(self)
    
    def log(self):
        return op_log(self)

    def backward(self):
        graph = []
        visited = set()

        def build_graph(v):
            if v not in visited:
                visited.add(v)
                for child in v._children:
                    build_graph(child)
                graph.append(v)

        build_graph(self)

        self._grad = 1.
        for v in reversed(graph):
            v._backward()

    def __repr__(self):
        return f"Scalar(data={self._scalar}, grad={self._grad}, {self._backward})"