from .scalar import Scalar
class ScalarSGD(object):
    def __init__(self, parameters, lr=0.001):
        super(object, self).__init__()
        self.parameters = parameters
        self.lr = lr
    
    def step(self):
        for p in self.parameters:
            p._scalar -= self.lr * p._grad

class ScalarLRScheduler(object):
    def __init__(self, optimizer):
        super(object, self).__init__()
        self.optimizer = optimizer
    
    def step(self, *args, **kwargs):
        raise NotImplementedError()

class LRLinearDecayScheduler(ScalarLRScheduler):
    def __init__(self, optimizer, lr=1, min_lr=0.1, last_epoch=-1, T=100):
        super(LRLinearDecayScheduler, self).__init__(optimizer)
        self.init_lr = lr
        self.decay = lr-min_lr
        self.last_epoch = last_epoch
        self.T = T
    
    def step(self):
        self.last_epoch += 1
        lr = self.init_lr - self.decay * self.last_epoch / self.T
        self.optimizer.lr = lr
