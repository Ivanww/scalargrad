# Auto Grad

**Targets of Learning Implementation of Auto Grad**

1. Learn the math of auto grad
2. Implement a "Scalar Grad" to as a demo for auto grad
3. Based on this toy lib, implement some machine learning algorithm (MLP)

**Math of Auto Grad (For scalar)**

0. Chain rule of function gradients
   $$
   G(x) = f(b)=f(g(x)) \\
   \frac{dG(x)}{dx} = \frac{df(g(x))}{dg(x)}\frac{dg(x)}{dx}
   $$
   Keep track of each function applied to $x$ and we can compute complex gradients on it. (How to track? Forward: build a computational graph. Backward: use topology sort on the computational graph)

1. Addition (+)

$$
\frac{\partial f(a+b)}{\partial a} = \frac{df}{d(a+b)}\cdot 1
$$

2. Multiplication (\*)
   $$
   \frac{\partial f(a*b)}{\partial a} = \frac{df}{d(a*b)}\cdot b
   $$
   

3. Pow (of plain number, $a^x$)
   $$
   \frac{\partial f(a^x)}{\partial a} = \frac{df}{d(a^x)}\cdot xa^{x-1}
   $$

Use the combination of above operator, we can build more operators like division (/), exp ($\exp$). Or we can build more "compressed" operator for better performance is we already the analytical gradients.

4. Exponent ($\exp(a)$)
   $$
   \frac{\partial f(\exp(a)}{\partial a} = \frac{df}{d\exp(a)}\cdot \exp(a)
   $$

5. 

5. Sigmoid ($\sigma(a)$)

$$
\frac{\partial f(\sigma(a))}{\partial a} = \frac{df}{d\sigma(a)}\cdot \sigma(a)(1-\sigma(a))
$$

A step further, by defining the backward function, we can add some "non-gradient" operator into the   computational graph

6. ReLU (ReLU(a))

$$
\frac{\partial f(ReLU(a))}{\partial a} = 
\begin{cases}
\frac{df(ReLU(a))}{dReLU(a)}, \text{if} \space ReLU(a) \ge 0\\
0, \text{otherwise}
\end{cases}
$$

**Implementation**

1. Auto grad can be implemented on any bottom layer computational library by wrap the original operator
2. For minimal demo purpose, use the original Python operator as the "computational lib". Using the Numpy, CUDA should follow the similar way.
3. Auto Grad Operator = Original operator + Backward function
4. `Scalar` class defines the "Node" for the computational graph
5. `op_xxx` functions define the new operators on `Scalar`

**Implementation MLP with this Tiny ScalarGrad**

1. `ScalarModule` manages a set of `Scalar`, which is similar to `torch.nn.Module`.
2. Based on `ScalarModule`, build `ScalarLinear`, `ScalarSequential` for high level abstraction of neural network
3. `ScalarSGD` is the optimizer for gradient defense optimization. 
4. Use `BCElossfunction` (similar to `torch.nn.BCELoss`) in the demo.