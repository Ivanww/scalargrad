import sys, os
sys.path.append('/home/ivan/workspace/scalargrad')
from vectorgrad.variable import Variable
import torch
import numpy as np

def test_sanity_check():
    x = Variable(-4.0)
    z = 2 * x + 2 + x
    q = z.relu() + z * x
    h = (z * z).relu()
    y = h + q + q * x
    y.backward()
    xmg, ymg = x, y

    x = torch.Tensor([-4.0]).double()
    x.requires_grad = True
    z = 2 * x + 2 + x
    q = z.relu() + z * x
    h = (z * z).relu()
    y = h + q + q * x
    y.backward()
    xpt, ypt = x, y

    # forward pass went well
    print(ymg.data, ypt.data.item())
    assert ymg.data == ypt.data.item()
    # backward pass went well
    print(xmg.grad, xpt.grad.item())
    assert xmg.grad == xpt.grad.item()

def test_sigmoid_op():
    x = np.random.uniform(0.5, 1.5, (100, 100))
    grad = np.random.uniform(0.5, 1.5, (100, 100))

    xv = Variable(x)
    sv = xv.sigmoid()
    sv.backward(grad)

    xt = torch.tensor(x, requires_grad=True)
    st = torch.sigmoid(xt)
    st.backward(gradient=torch.from_numpy(grad))

    print('Sigmoid Forward (Max) Error:',)
    print(np.abs(sv.data - st.detach().numpy()).max())
    print('Sigmoid Backward (Max) Error:',)
    print(np.abs(xv.grad-xt.grad.numpy()).max())
    print(xv.saved_values)

def test_log_op():
    x = np.random.uniform(0.5, 1.5, (100, 100))
    grad = np.random.uniform(0.5, 1.5, (100, 100))

    xv = Variable(x)
    lv = xv.log()
    lv.backward(grad)

    xt = torch.tensor(x, requires_grad=True)
    lt = torch.log(xt)
    lt.backward(gradient=torch.from_numpy(grad))

    print('Sigmoid Forward (Max) Error:',)
    print(np.abs(lv.data - lt.detach().numpy()).max())
    print('Sigmoid Backward (Max) Error:',)
    print(np.abs(xv.grad-xt.grad.numpy()).max())

def _test_multi_op(at, bt):
    mt = at * bt
    gradt = torch.randn(*mt.shape)
    mt.backward(gradient=gradt)

    av = Variable(at.detach().numpy())
    bv = Variable(bt.detach().numpy())
    mv = av * bv
    mv.backward(grad=gradt.numpy())

    print('broadcast forward error:', )
    print(np.abs(mv.data-mt.detach().numpy()).max())
    print('broadcast backward error:', )
    print(np.abs(av.grad-at.grad.numpy()).max(), )
    print(np.abs(bv.grad-bt.grad.numpy()).max())

def test_multi_op():
    at = torch.randn(2, 3, requires_grad=True)
    bt = torch.randn(3, requires_grad=True)
    _test_multi_op(at, bt)

    at = torch.randn(3, 1, requires_grad=True)
    bt = torch.randn(1, 3, requires_grad=True)
    _test_multi_op(at, bt)


def test_sum_op():
    x = np.random.uniform(-0.5, 0.5, (100, 100))
    xv = Variable(x)
    sum_xv = xv.sum()
    print('SUM OP result:', sum_xv.data)

    sum_xv.backward()
    print('SUM OP gradient shape: ', xv.grad.shape)

def test_mean_op():
    x = np.random.uniform(-0.5, 0.5, (100,))
    xv = Variable(x)
    mean_xv = xv.mean()

    print('Mean OP result shape: ', mean_xv.data.shape)
    mean_xv.backward()
    print('Mean OP gradient shape: ', xv.grad.shape)

def test_sum_non_op():
    xs = [Variable(np.random.uniform(0, 1, (1, 10))) for _ in range(10)]
    sum_xs = [x.sum() for x in xs]

    total = sum(sum_xs)
    total.backward()

    print(total)
    for x in xs:
        print(x.grad)

from vectorgrad.variable import _matmul_op
def test_dot_op():
    w = np.random.uniform(-0.05, 0.05, (2, 16))
    b = np.random.uniform(-0.05, 0.05, (1, 16))
    x = np.random.uniform(-0.05, 0.05, (100, 2))

    wv = Variable(w)
    bv = Variable(b)
    xv = Variable(x)

    # out = xv.dot(wv) + bv
    out = _matmul_op(xv, wv) + bv

    wt = torch.tensor(w, requires_grad=True)
    bt = torch.tensor(b, requires_grad=True)
    xt = torch.tensor(x, requires_grad=True)
    tout = torch.matmul(xt, wt) + bt

    grad = np.random.uniform(-0.05, 0.05, (100, 16))
    tgrad = torch.from_numpy(grad)

    out.backward(grad)
    tout.backward(gradient=tgrad)

    print(np.abs(out.data - tout.detach().numpy()).max())
    print(np.abs(wv.grad - wt.grad.numpy()).max())


import torch.nn.functional as F
def test_conv():
    x = np.random.randn(64, 28, 28, 1)
    strided_shape = 64, 26, 26, 3, 3, 1
    strided_strides = x.strides[:3] + x.strides[1:]
    strided = np.lib.stride_tricks.as_strided(x, strided_shape,
        strides=strided_strides)
    strided = strided.copy()
    strided = np.moveaxis(strided, (3, 4, 5), (1, 2, 3))
    strided = strided.reshape(64, 9, 26*26)

    xt = torch.from_numpy(x).permute(0, 3, 1, 2).contiguous()
    unfold = F.unfold(xt, 3)
    unfold = unfold.numpy()

    print(np.abs(strided-unfold).max())

from vectorgrad.variableConv import _unfold
from vectorgrad.variableConv import _conv2d_op
def test_conv_op():
    xt = torch.randn(64, 1, 28, 28, requires_grad=True)
    kt = torch.randn(16, 1, 3, 3, requires_grad=True)
    convt = F.conv2d(xt, kt, padding=1)

    grad = torch.randn_like(convt)
    convt.backward(grad)
    
    xn = xt.detach().numpy()
    kn = kt.detach().numpy()
    gradn = grad.numpy()

    xv = Variable(xn)
    kv = Variable(kn)
    convv = _conv2d_op(xv, kv)
    convv.backward(gradn)

    print('Conv2D forward pass error: ',)
    print(np.abs(convt.detach().numpy()-convv.data).max())
    print('Conv2D kernel grad error: ',)
    print(np.abs(kt.grad.numpy()-kv.grad).max())
    print('Conv2D inputs grad error: ',)
    print(np.abs(xt.grad.numpy()-xv.grad).max())

from vectorgrad.variableConv import _max_pool_op
def test_maxpool_op():
    xt = torch.randn(64, 1, 28, 28, requires_grad=True)
    poolt = F.max_pool2d(xt, kernel_size=2)

    grad = torch.randn_like(poolt)
    poolt.backward(grad)

    xn = xt.detach().numpy()
    gradn = grad.numpy()

    xv = Variable(xn)
    poolv = _max_pool_op(xv)
    poolv.backward(gradn)

    print('Maxpool forward pass error: ',)
    print(np.abs(poolt.detach().numpy()-poolv.data).max())
    print('Maxpool backward pass error: ',)
    print(np.abs(xt.grad.numpy()-xv.grad).max())

from vectorgrad.variableConv import _mean_pool_op
def test_meanpool_op():
    xt = torch.randn(64, 1, 28, 28, requires_grad=True)
    poolt = F.avg_pool2d(xt, kernel_size=2)

    grad = torch.randn_like(poolt)
    poolt.backward(grad)

    xn = xt.detach().numpy()
    gradn = grad.numpy()

    xv = Variable(xn)
    poolv = _mean_pool_op(xv)
    poolv.backward(gradn)

    print('Meanpool forward pass error: ',)
    print(np.abs(poolt.detach().numpy()-poolv.data).max())
    print('Meanpool backward pass error: ',)
    print(np.abs(xt.grad.numpy()-xv.grad).max())

def conv_exp():
    xt = torch.randn(16, 1, 28, 28, requires_grad=True)
    kt = torch.randn(16, 1, 3, 3, requires_grad=True)
    convt = F.conv2d(xt, kt, padding=1)

    grad = torch.randn_like(convt)
    convt.backward(grad)

    xdt = xt.detach()
    kdt = kt.detach()
    convdt = convt.detach()

    kt_grad = torch.matmul(
        grad.view(convdt.size(0), convdt.size(1), -1),
        F.unfold(xdt, 3, padding=1).permute(0, 2, 1))
    kt_grad = kt_grad.sum(dim=0).view(16, 1, 3, 3)
    print(kt_grad.shape)
    print((kt.grad-kt_grad).abs().max())

    flip_k = torch.flip(kdt, (2, 3)).permute(1, 0, 2, 3)
    xt_grad = F.conv2d(F.pad(grad, (1, 1, 1, 1)), flip_k)
    print(xt_grad.shape)
    print((xt.grad-xt_grad).abs().max())


if __name__ == '__main__':
    # test_sanity_check()
    # test_log_sigmoid_op()
    # test_sum_op()
    # test_mean_op()
    # test_multi_op()
    # test_log_op()
    # test_sum_non_op()
    # test_dot_op()
    # test_sigmoid_op()
    # test_conv()
    # test_conv_op()
    # conv_exp()
    # test_maxpool_op()
    test_meanpool_op()
