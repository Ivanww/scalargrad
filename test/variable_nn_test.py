import sys, os
sys.path.append('/home/ivan/workspace/scalargrad')
from vectorgrad.variable import Variable
from vectorgrad.variableNN import VariableLinearLayer
from vectorgrad.variableNN import VariableSequentialModule
from vectorgrad.variableOptim import VariableSGD
from vectorgrad.variableOptim import LRLinearDecayScheduler

import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons, make_blobs
from tqdm import tqdm

def bce_loss(m, x, t):
    loss_pos = - (t * x.log())
    loss_neg = - ((1-t) * (1-x).log())
    # loss = - (t * x.log() + (1-t) * (1-x).log())
    loss = loss_pos + loss_neg
    data_loss = loss.mean()

    # FIXME: shuold be handled by optimizer
    alpha = 1e-4
    reg_loss = alpha * sum([(p*p).sum() for p in m.parameters()])
    total_loss = data_loss + reg_loss

    return total_loss

def test_variable_nn():
    np.random.seed(1337)
    random.seed(1337)
    X, y = make_moons(n_samples=100, noise=0.1)

    model = VariableSequentialModule([
        VariableLinearLayer(2, 16, act='relu'),
        VariableLinearLayer(16, 16, act='relu'),
        VariableLinearLayer(16, 1, act='sigmoid')
    ])
    optimizer = VariableSGD(model.parameters(), lr=0.05)
    scheduler = LRLinearDecayScheduler(optimizer)

    for epoch in tqdm(range(100)):
        Xv = Variable(X)
        Yv = Variable(y.reshape((-1, 1)))

        out = model(Xv)
        loss = bce_loss(model, out, Yv)

        matched = (out.data[:, 0] > 0.5) == (y > 0.5)

        acc = matched.sum() / len(matched)

        model.zero_grad()
        loss.backward()
        optimizer.step()
        scheduler.step()

        tqdm.write("step {} loss {:.6f}, accuracy {} %".format(epoch, loss.data, acc * 100))

from vectorgrad.variableConv import VariableConv2d


if __name__ == '__main__':
    test_variable_nn()