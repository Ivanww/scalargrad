import sys, os
sys.path.append('/Users/ivanw/workspace/scalargrad')
from scalargrad.scalar import Scalar
import torch

def test_sanity_check():
    x = Scalar(-4.0)
    z = 2 * x + 2 + x
    q = z.relu() + z * x
    h = (z * z).relu()
    y = h + q + q * x

    y.backward()
    xmg, ymg = x, y

    x = torch.Tensor([-4.0]).double()
    x.requires_grad = True
    z = 2 * x + 2 + x
    q = z.relu() + z * x
    h = (z * z).relu()
    y = h + q + q * x
    y.backward()
    xpt, ypt = x, y

    # forward pass went well
    print(ymg._scalar, ypt.data.item())
    assert ymg._scalar == ypt.data.item()
    # backward pass went well
    print(xmg._grad, xpt.grad.item())
    assert xmg._grad == xpt.grad.item()

def test_more_ops():
    a = Scalar(-4.0)
    b = Scalar(2.0)
    c = a + b
    d = a * b + b**3
    c += c + 1
    c += 1 + c + (-a)
    d += d * 2 + (b + a).relu()
    d += 3 * d + (b - a).relu()
    e = c - d
    f = e**2
    g = f / 2.0
    g += 10.0 / f
    g.backward()
    amg, bmg, gmg = a, b, g

    a = torch.Tensor([-4.0]).double()
    b = torch.Tensor([2.0]).double()
    a.requires_grad = True
    b.requires_grad = True
    c = a + b
    d = a * b + b**3
    c = c + c + 1
    c = c + 1 + c + (-a)
    d = d + d * 2 + (b + a).relu()
    d = d + 3 * d + (b - a).relu()
    e = c - d
    f = e**2
    g = f / 2.0
    g = g + 10.0 / f
    g.backward()
    apt, bpt, gpt = a, b, g

    tol = 1e-6
    # forward pass went well
    print(gmg._scalar, gpt.data.item())
    assert abs(gmg._scalar - gpt.data.item()) < tol
    # backward pass went well
    print(amg._grad, apt.grad.item())
    assert abs(amg._grad - apt.grad.item()) < tol
    print(bmg._grad, bpt.grad.item())
    assert abs(bmg._grad - bpt.grad.item()) < tol

import random
def test_sigmoid():
    n = random.uniform(-1, 1)
    
    a = Scalar(n)
    s = a.sigmoid()
    s.backward()

    asg, ssg = a, s

    a = torch.Tensor([n]).double()
    a.requires_grad = True
    s = torch.sigmoid(a)
    s.backward()
    
    apt, spt = a, s

    tol = 1e-6
    print(ssg._scalar, spt.data.item())
    assert abs(ssg._scalar - spt.data.item()) < tol
    print(asg._grad, apt.grad.item())
    assert abs(asg._grad - apt.grad.item()) < tol

def test_log():
    n = random.uniform(0, 1)
    
    a = Scalar(n)
    s = a.log()
    s.backward()

    asg, ssg = a, s

    a = torch.Tensor([n]).double()
    a.requires_grad = True
    s = torch.log(a)
    s.backward()
    
    apt, spt = a, s

    tol = 1e-6
    print(ssg._scalar, spt.data.item())
    assert abs(ssg._scalar - spt.data.item()) < tol
    print(asg._grad, apt.grad.item())
    assert abs(asg._grad - apt.grad.item()) < tol


if __name__ == "__main__":
    test_sanity_check()
    test_more_ops()
    test_sigmoid()
    test_log()