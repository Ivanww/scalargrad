import sys, os
sys.path.append('/home/ivan/workspace/scalargrad')

import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons, make_blobs

from scalargrad.scalar import Scalar
from scalargrad.scalarNN import ScalarSequentialModule
from scalargrad.scalarNN import ScalarLinear
from scalargrad.scalarOptim import ScalarSGD
from scalargrad.scalarOptim import LRLinearDecayScheduler

def forward(m, x, t):
    outs = list(map(m, x))
    losses = [(1 + -ti*outi).relu() for ti, outi in zip(t, outs)]
    data_loss = sum(losses) * (1.0 / len(losses))

    alpha = 1e-4
    reg_loss = alpha * sum((p*p for p in m.parameters()))
    total_loss = data_loss + reg_loss

    return total_loss, outs

def bce_loss(m, x, t):
    outs = list(map(m, x))
    losses = [-(ti * outi.log() + (1-ti) * (1 - outi).log()) for ti, outi in zip(t, outs)]
    data_loss = sum(losses) * (1.0 / len(losses))

    alpha = 1e-4
    reg_loss = alpha * sum((p*p for p in m.parameters()))
    total_loss = data_loss + reg_loss

    return total_loss, outs

def test_scalar_nn():
    np.random.seed(1337)
    random.seed(1337)
    X, y = make_moons(n_samples=100, noise=0.1)

    # y = y*2 - 1
    plt.figure(figsize=(5,5))
    plt.scatter(X[:,0], X[:,1], c=y, s=20, cmap='jet')
    plt.savefig('moon_dataset.png')

    modules = [
        ScalarLinear(2, 16),
        ScalarLinear(16, 16),
        ScalarLinear(16, 1, act='sigmoid')
    ]
    mlp = ScalarSequentialModule(modules)
    optimizer = ScalarSGD(mlp.parameters(), lr=0.05)
    scheduler = LRLinearDecayScheduler(optimizer)

    # train mlp
    batch_size = None

    for epoch in range(100):
        if batch_size is None:
            Xb, yb = X, y
        else:
            ri = np.random.permutation(X.shape[0])[:batch_size]
            Xb, yb = X[ri], y[ri]

        inputs = [list(map(Scalar, xrow)) for xrow in Xb] # list of xi
        # losses, pred = forward(mlp, inputs, yb)
        losses, pred = bce_loss(mlp, inputs, yb)

        matched = [(yi > 0.5) == (outi._scalar > 0.5) for yi, outi in zip(yb, pred)]
        acc = sum(matched) / len(matched)

        mlp.zero_grad()
        losses.backward()
        optimizer.step()
        scheduler.step()

        print(f"step {epoch} loss {losses._scalar}, accuracy {acc*100}%")

    # visualize decision boundary

    h = 0.25
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                        np.arange(y_min, y_max, h))
    Xmesh = np.c_[xx.ravel(), yy.ravel()]
    inputs = [list(map(Scalar, xrow)) for xrow in Xmesh]
    scores = list(map(mlp, inputs))
    Z = np.array([s._scalar > 0.5 for s in scores])
    Z = Z.reshape(xx.shape)

    fig = plt.figure()
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral, alpha=0.8)
    plt.scatter(X[:, 0], X[:, 1], c=y, s=40, cmap=plt.cm.Spectral)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.savefig('moon_demo.png')

if __name__ == "__main__":
    test_scalar_nn()
