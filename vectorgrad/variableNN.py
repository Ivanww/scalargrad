import numpy as np
from .variable import Variable

class VariableModule(object):
    def __init__(self):
        super(VariableModule, self).__init__()

    def parameters(self):
        return []

    def zero_grad(self):
        for p in self.parameters():
            p.zero_grad()

    def __call__(self, x):
        return self.forward(x)

    def forward(self, x):
        raise NotImplemented()

from .variable import _dot_op
class VariableLinearLayer(VariableModule):
    def __init__(self, in_channels, out_channels, use_bias=True, act=None):
        # std = 1 / in_channels
        self.w = Variable(np.random.uniform(-1, 1, (in_channels, out_channels)))
        if use_bias:
            self.b = Variable(np.zeros((out_channels,)))
        else:
            self.b = None
        self.act = act

    def parameters(self):
        if self.b is None:
            return [self.w]
        else:
            return [self.w, self.b]

    def forward(self, x):
        out = _dot_op(x, self.w)
        if self.b is not None:
            out = out + self.b
        if self.act == 'sigmoid':
            return out.sigmoid()
        elif self.act == 'relu':
            return out.relu()
        else:
            return out

    def __repr__(self):
        return f'Linear [W shape: {self.w.data.shape}, \
            b shape: {self.b if self.b is None else self.b.data.shape}]'

class VariableSequentialModule(VariableModule):
    def __init__(self, modules):
        super(VariableSequentialModule, self).__init__()
        self.modules = modules

    def forward(self, x):
        for m in self.modules:
            x = m(x)
        return x

    def parameters(self):
        params = []
        for m in self.modules:
            params.extend(m.parameters())
        return params

    def __repr__(self):
        return f"Sequential of [\n{''.join([str(m) for m in self.modules])}]"


if __name__ == '__main__':
    l = VariableLinearLayer(3, 5)

