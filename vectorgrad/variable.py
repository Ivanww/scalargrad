import numpy as np

def variable_check(num_var):
    def decorator(func):
        def checked_func(*args):
            for arg in args[:num_var]:
                if not isinstance(arg, Variable):
                    raise TypeError(f'Need Variable Type for Operators, But Got {type(arg)}')
            return func(*args)
        return checked_func
    return decorator

def _broadcast_op(a, shape):
    def _backward(grad):
        data_shape = a.data.shape
        assert len(data_shape) <= len(shape)
        if len(data_shape) < len(shape):
            broadcast_shape = (1,) * (len(shape)-len(data_shape)) + data_shape
        else:
            broadcast_shape = data_shape
        broadcast_axis = tuple(np.where(np.logical_and(
            np.array(broadcast_shape) == 1,
            np.array(shape) > 1
        ))[0])
        a.grad += np.reshape(np.sum(grad, axis=broadcast_axis), data_shape)
    out = Variable(np.broadcast_to(a.data, shape), is_leaf=False,
        children=[a], backward_fn=_backward, op='broadcast')
    return out

def broadcast_check(num_var):
    def decorator(func):
        def checked_func(*args):
            b = np.broadcast(*[arg.data for arg in args[:num_var]])
            checked_args = [arg if arg.data.shape == b.shape else _broadcast_op(arg, b.shape)
                for arg in args[:num_var]]
            checked_args += args[num_var:]
            return func(*checked_args)
        return checked_func
    return decorator

def constant_check(func):
    def checked_func(*args):
        assert isinstance(args[0], Variable)
        checked_args = [args[0]]
        dim = len(args[0].data.shape)
        for arg in args[1:]:
            if isinstance(arg, Variable):
                checked_args.append(arg)
            elif np.isscalar(arg):
                checked_args.append(np.array(arg).reshape([1]*dim))
            else:
                raise TypeError(f'Constant Value Should be a scalar')
        return func(*checked_args)
    return checked_func


@variable_check(2)
@broadcast_check(2)
def _add_op(a, b):
    def _backward(grad):
        a.grad += grad
        b.grad += grad

    out = Variable(a.data + b.data, is_leaf=False,
        children=[a, b], backward_fn=_backward, op='+')
    return out

@variable_check(1)
@constant_check
def _c_add_op(a, b):
    def _backward(grad):
        a.grad += grad

    out = Variable(a.data + b.data, is_leaf=False,
        children=[a], backward_fn=_backward, op='+c')
    return out

@variable_check(2)
@broadcast_check(2)
def _mul_op(a, b):
    def _backward(grad):
        a.grad += grad * b.data
        b.grad += grad * a.data
    out = Variable(a.data * b.data, is_leaf=False, backward_fn=_backward,
        children=[a, b], op='*')
    return out

@variable_check(1)
@constant_check
def _c_mul_op(a, b):
    def _backward(grad):
        a.grad += grad * b
    out = Variable(a.data * b, is_leaf=False, children=[a],
        backward_fn=_backward, op='*c')
    return out

@variable_check(1)
def _relu_op(a):
    def _backward(grad):
        a.grad[a.data>0] += grad[a.data>0]

    out = Variable(np.maximum(a.data, 0), is_leaf=False, children=[a],
        backward_fn=_backward, op='relu')
    return out

@variable_check(1)
def _sigmoid_op(a):
    # BUG: operations needs to save some values
    def _backward(grad):
        out_data = a.saved_values.pop('logits')
        a.grad += out_data * (1 - out_data) * grad
    sig = 1 / (1 + np.exp(-a.data))
    a.saved_values.update({'logits': sig})

    out = Variable(sig.copy(), is_leaf=False, children=[a],
        backward_fn=_backward, op='sigmoid')
    return out

@variable_check(1)
def _pow_op(a, b):
    def _backward(grad):
        a.grad += (b * a.data**(b-1)) * grad
    out = Variable(a.data**b, is_leaf=False, children=[a],
        backward_fn=_backward, op='pow')

@variable_check(2)
def _dot_op(a, b):
    def _backward(grad):
        a.grad += np.dot(grad, b.data.T)
        b.grad += np.dot(a.data.T, grad)
    out = Variable(np.dot(a.data, b.data), is_leaf=False, children=[a, b],
        backward_fn=_backward, op='dot')
    return out

@variable_check(2)
def _matmul_op(a, b):
    def _backward(grad):
        a.grad += np.matmul(grad, np.moveaxis(b.data, (-1, -2), (-2, -1)))
        b.grad += np.matmul(np.moveaxis(a.data, (-1, -2), (-2, -1)), grad)
    out = Variable(np.matmul(a.data, b.data), is_leaf=False, children=[a, b],
        backward_fn=_backward, op='matmul')
    return out

@variable_check(1)
def _log_op(a):
    def _backward(grad):
        a.grad += 1 / a.data * grad
    out = Variable(np.log(a.data), is_leaf=False, children=[a],
        backward_fn=_backward, op='log')

    return out

@variable_check(1)
def _sum_op(a):
    def _backward(grad):
        a.grad += grad
    out = Variable(a.data.sum(), is_leaf=False, children=[a],
        backward_fn=_backward, op='sum')
    # print('sum op called, input shape: {}, output shape: {}'.format(a.data.shape, out.data.shape))
    return out

class Variable(object):
    def __init__(self, data, is_leaf=True, children=[], backward_fn=None, op=''):
        super().__init__()

        self.data = np.array(data, dtype=np.float32)
        self.is_leaf = is_leaf
        self.children = children
        self.op = op
        self.backward_fn = backward_fn
        self.saved_values = {}

        self.grad = None
        self.zero_grad()

    def zero_grad(self):
        self.grad = np.zeros_like(self.data)

    def dot(self, other):
        return _dot_op(self, other)

    def __add__(self, other):
        if isinstance(other, (int, float)):
            return _c_add_op(self, other)
        else:
            return _add_op(self, other)

    def __radd__(self, other):
        if isinstance(other, (int, float)):
            # print('call acc_c, other operand is ', other)
            return _c_add_op(self, other)
        else:
            return _add_op(other, self)

    def __mul__ (self, other):
        if isinstance(other, (int, float)):
            return _c_mul_op(self, other)
        else:
            return _mul_op(self, other)

    def __rmul__(self, other):
        if isinstance(other, (int, float)):
            return _c_mul_op(self, other)
        else:
            return _mul_op(other, self)

    def __neg__(self):
        return _c_mul_op(self, -1)

    def __sub__(self, other):
        return self + (-other)

    def __rsub__(self, other):
        return other + (-self)

    def __truediv__(self, other):
        return self * (other ** -1)

    def __rtruediv__(self, other):
        return other * (self**-1)

    def __repr__(self):
        return f"Variable(data={self.data}, grad={self.grad}, num_child={len(self.children)}, {self.backward_fn})"

    def relu(self):
        return _relu_op(self)

    def sigmoid(self):
        return _sigmoid_op(self)

    def log(self):
        return _log_op(self)

    def sum(self):
        return _sum_op(self)

    def mean(self):
        return _sum_op(self) / self.data.size

    def backward(self, grad=None):
        post_order = []
        visited = set()

        def _build(v):
            if v not in visited:
                visited.add(v)
                for c in v.children:
                    _build(c)
                post_order.append(v)
        _build(self)

        if grad is None:
            self.grad = np.ones_like(self.data)
        else:
            self.grad = grad

        for v in reversed(post_order):
            if not v.is_leaf:
                # print('backward OP: [{}],  data shape: {}, grad shape: {}'.format(
                    # v.op, v.data.shape, v.grad.shape))
                v.backward_fn(v.grad)
            v.children = []

if __name__ == '__main__':
    a = Variable(2)
    b = Variable(3)
    print(a, b)
    n1 = a * 2 # 4
    n2 = n1 + b # 7
    n3 = n2 * n2 # 49
    n3.backward()

    for x in [a, b, n1, n2, n3]:
        print(x)
