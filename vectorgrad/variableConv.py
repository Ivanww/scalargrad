from .variable import Variable
from .variable import variable_check
from .variable import _dot_op
from .variable import _matmul_op
import numpy as np

@variable_check(1)
def _reshape_op(a, new_shape):
    def _backward(grad):
        old_shape = a.data.shape
        a.grad += np.reshape(grad, old_shape)

    out = Variable(np.reshape(a.data, new_shape), is_leaf=False,
        children=[a], backward_fn=_backward, op='reshape')

    return out

@variable_check(1)
def _permute_op(a, src_axis, dst_axis):
    def _backward(grad):
        a.grad += np.moveaxis(grad, dst_axis, src_axis)
    out = Variable(np.moveaxis(a.data, src_axis, dst_axis), is_leaf=False,
        children=[a], backward_fn=_backward, op='moveaxis') 
    
    return out

@variable_check(1)
def _pad_op(a, pad_width):
    def _backward(grad):
        mask = np.ones(a.shape)
        mask = np.pad(mask, pad_width)
        a.grad += grad[mask==1]
    
    out = Variable(np.pad(a.data, pad_width), is_leaf=False,
        children=[a], backward_fn=_backward, op='pad')

def _unfold(x, kernel_size, pad=1, stride=1):
    b, c, h, w = x.shape
    kh, kw = kernel_size

    data = np.pad(x, ((0, 0), (0, 0), (pad, pad), (pad, pad)))
    unfold = np.lib.stride_tricks.sliding_window_view(data,
        kernel_size, axis=(2, 3))
    unfold = np.moveaxis(unfold, (0, 1, 2, 3, 4, 5), (0, 1, 4, 5, 2, 3))
    unfold = np.reshape(unfold, (b, c*kw*kh, h-kh+2*pad+1, w-kw+2*pad+1))
    unfold = np.reshape(unfold[:, :, ::stride, ::stride], (b, c*kw*kh, -1))

    return unfold

def _conv_op(x, k, pad, stride):
    b, _, inW, inH = x.shape
    c, _, kW, kH = k.shape

    outW = (inW - kW + 2*pad) // stride + 1
    outH = (inH - kH + 2*pad) // stride + 1

    unfolded = _unfold(x, (kW, kH), pad, stride)
    kernel = k.reshape((k.shape[0], -1))

    # conv = np.matmul(np.moveaxis(unfolded, -1, -2), kernel.T)
    # conv = np.moveaxis(conv, -1, -2)
    conv = np.matmul(kernel, unfolded)
    conv = np.reshape(conv, (b, c, outW, outH))

    return conv, unfolded

@variable_check(1)
def _max_pool_op(a, psize=2):
    assert len(a.data.shape) == 4
    assert isinstance(psize, int)

    b, c, h, w = a.data.shape

    def _backward(grad):
        max_idx = a.saved_values.pop('max_idx')
        _, _, gh, gw = grad.shape
        a_grad = np.zeros((b*c*gh*gw, psize*psize))
        a_grad[np.arange(b*c*gh*gw), max_idx.ravel()] = grad.ravel()
        a_grad = np.reshape(a_grad, (b, c, gh, gw, psize, psize))
        a_grad = np.moveaxis(a_grad, 3, 4)
        a_grad = np.reshape(a_grad, (b, c, psize*gh, psize*gw))
        a.grad += a_grad

    unfolded = _unfold(a.data, (psize, psize), pad=0, stride=psize)
    unfolded = np.reshape(unfolded, (b, c, psize*psize, -1))
    max_idx = np.argmax(unfolded, axis=2).ravel()
    a.saved_values.update({'max_idx': max_idx})
    
    pooled = np.max(unfolded, axis=2, keepdims=True)
    pooled = np.reshape(pooled, (b, c, h // psize, w // psize))
    out = Variable(pooled, is_leaf=False, children=[a],
        backward_fn=_backward, op='maxpool') 
    return out

@variable_check(1)
def _mean_pool_op(a, psize=2):
    assert len(a.data.shape) == 4
    assert isinstance(psize, int)

    b, c, h, w = a.data.shape

    def _backward(grad):
        _, _, gh, gw = grad.shape
        a_grad = np.zeros((b*c*gh*gw, psize*psize))
        a_grad += np.moveaxis(np.expand_dims(grad, -1), -1, 2) # * (psize*psize)
        a_grad = np.reshape(a_grad, (b, c, psize, psize, gh, gw))
        a_grad = np.moveaxis(a_grad, (4, 5), (2, 4))
        a_grad = np.reshape(a_grad, (b, c, psize*gh, psize*gw))
        a.grad += a_grad

    unfolded = _unfold(a.data, (psize, psize), pad=0, stride=psize)
    unfolded = np.reshape(unfolded, (b, c, psize*psize, -1))
    
    pooled = np.mean(unfolded, axis=2, keepdims=True)
    pooled = np.reshape(pooled, (b, c, h // psize, w // psize))
    out = Variable(pooled, is_leaf=False, children=[a],
        backward_fn=_backward, op='meanpool') 
    return out



@variable_check(2)
def _conv2d_op(x, k, pad=1, stride=1):
    assert len(x.data.shape) == 4
    assert len(k.data.shape) == 4
    assert isinstance(pad, int)
    assert isinstance(stride, int)

    def _backward(grad):
        unfolded = x.saved_values.pop('unfolded')
        k_grad = np.matmul(np.reshape(grad, (grad.shape[0], grad.shape[1], -1)),
            np.moveaxis(unfolded, -1, -2))
        k_grad = np.reshape(k_grad.sum(axis=0), k.data.shape)
        k.grad += k_grad

        flip_k = np.flip(k.data, (2, 3))
        flip_k = np.moveaxis(flip_k, 0, 1)
        x_grad, _ =  _conv_op(grad, flip_k, pad, stride)
        x.grad += x_grad

    conv, unfolded = _conv_op(x.data, k.data, pad, stride)
    x.saved_values.update({'unfolded': unfolded})
    out = Variable(conv, is_leaf=False, children=[x, k],
        backward_fn=_backward, op='conv2d')
    return out

from .variableNN import VariableModule
class VariableConv2d(VariableModule):
    def __init__(self, in_channel, out_channel, kernel_size, pad, stride, use_bias=False):
        super().__init__()
        self.w = Variable(np.random.randn(out_channel, in_channel, kernel_size, kernel_size))
        if use_bias:
            self.b = Variable(np.zeros((out_channel, 1, 1)))
        else:
            self.b = None
    
    def parameters(self):
        if self.b is None:
            return [self.w]
        else:
            return [self.w, self.b]
    
    def forward(self, x):
        out = _conv2d_op(x, self.k)
        if self.b is not None:
            out = out + self.b
